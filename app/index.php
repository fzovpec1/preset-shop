<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
	<link rel="stylesheet" href="css/main.min.css">
	<title>Preset Shop</title>
</head>
<body>
	<div class="header">
      <input type="checkbox" id="chk">
      <label for="chk" class="show-menu-btn">
        <i class="fas fa-bars"></i>
      </label>
      <ul class="menu">
        <a href="index.php">Инстаграм</a>
        <a href="instruction.php">Как установить</a>
        <a href="about_author.php">Об авторе</a>
		<a href="#">Отзывы</a>
        <a href="#">Контакты</a>
		<a href="#">ENG</a>
        <label for="chk" class="hide-menu-btn">
          <i class="fas fa-times"></i>
        </label>
      </ul>
  </div>
  <div class="container main-content">
  	<div class="row">
			<?php
					$connect = mysqli_connect('localhost', 'id5859679_fzovpec', '1702spb', 'id5859679_localhost');
					$query = "SELECT * from commodities";
        	$result = mysqli_query($connect, $query);
					$rs = mysqli_fetch_array($result);
					$row = $rs;
					while($row = mysqli_fetch_array($result)){
							echo '
								<div class="col-md-6 images" style = "margin-bottom: 100px;">
								<div class="col-md-12 big">
									<img class="col-xs-6 col-md-6" src="admin/'.$row['photo_1'].'" alt="" height = "217px">
									<img class="col-xs-6 col-md-6" src="admin/'.$row['photo_2'].'" alt="" height = "217px">
									<div class="text">
										<h4>'.$row['short_name'].'</h4>
										<p>'.$row['short_description'].'</p>
									</div>
									<img class="col-xs-6 col-md-6" src="admin/'.$row['photo_3'].'" alt="" height = "217px">
									<img class="col-xs-6 col-md-6" src="admin/'.$row['photo_4'].'" alt="" height = "217px">
								</div>
								<div class="col-md-12 small">
									<img class="col-xs-3 col-md-3" src="admin/'.$row['photo_1'].'" alt="" height = "100px">
									<img class="col-xs-3 col-md-3" src="admin/'.$row['photo_2'].'" alt="" height = "100px">
									<img class="col-xs-3 col-md-3" src="admin/'.$row['photo_3'].'" alt="" height = "100px">
									<img class="col-xs-3 col-md-3" src="admin/'.$row['photo_4'].'" alt="" height = "100px">
								</div>
								</div>
							<div class="col-md-6 description" style = "height: 700px">
								<h2><b>'.$row['name'].'</b></h2>
								<h3>'.$row['price'].'руб</h3>
								<button type="button" name="button">
									Купить
								</button>
								<p>'.$row['description'].'</p>
							</div>
							</div>
							';
					}
			?>
  </div>
</body>
</html>
