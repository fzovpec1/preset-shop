<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
	<link rel="stylesheet" href="css/main.min.css">
	<title>Preset Shop</title>
</head>
<body>
    <form method = 'POST' enctype = 'multipart/form-data'>
        <input placeholder = 'Название товара' name = 'name'><br>
        <input placeholder = 'Краткое название товара' name = 'short_name'><br>
        <input placeholder = 'Цена' name = 'price' type = 'number'><br>
        <input placeholder = 'Описание' name = 'description'><br>
        <input placeholder = 'Краткое описание' name = 'short_description'><br>
        <span>Фото 1</span>
        <input value = 'Фото 1' type = 'file' name = 'photo-1'><br>
        <span>Фото 2</span>
        <input value = 'Фото 2' type = 'file' name = 'photo-2'><br>
        <span>Фото 3</span>
        <input value = 'Фото 3' type = 'file' name = 'photo-3'><br>
        <span>Фото 4</span>
        <input value = 'Фото 4' type = 'file' name = 'photo-4'><br>
        <input type = 'submit' name = 'add'>
    </form>
    <?php
        $connect = mysqli_connect('localhost', 'id5859679_fzovpec', '1702spb', 'id5859679_localhost');
        if(isset($_POST['name'])){
            $name = $_POST['name'];
            $short_name = $_POST['short_name'];
            $price = $_POST['price'];
            $description = $_POST['description'];
            $short_description = $_POST['short_description'];
            $path = 'img/';
            $ext_1 = $_FILES['photo-1']['name'];
            $new_name_1 = time().'.'.$ext_1;
            $ext_2 = $_FILES['photo-2']['name'];
            $new_name_2 = time().'.'.$ext_2;
            $ext_3 = $_FILES['photo-3']['name'];
            $new_name_3 = time().'.'.$ext_3;
            $ext_4 = $_FILES['photo-4']['name'];
            $new_name_4 = time().'.'.$ext_4;
            $full_path_1 = $path.$new_name_1;
            $full_path_2 = $path.$new_name_2;
            $full_path_3 = $path.$new_name_3;
            $full_path_4 = $path.$new_name_4;
            if($_FILES['photo-1']['error'] == 0){
                if(move_uploaded_file($_FILES['photo-1']['tmp_name'], $full_path_1)){}
            }
            if($_FILES['photo-2']['error'] == 0){
                if(move_uploaded_file($_FILES['photo-2']['tmp_name'], $full_path_2)){}
            }
            if($_FILES['photo-3']['error'] == 0){
                if(move_uploaded_file($_FILES['photo-3']['tmp_name'], $full_path_3)){}
            }
            if($_FILES['photo-4']['error'] == 0){
                if(move_uploaded_file($_FILES['photo-4']['tmp_name'], $full_path_4)){}
            }
            $query = "INSERT into commodities (name, price, description, short_description, photo_1, photo_2, photo_3, photo_4, short_name) 
                VALUES ('$name', '$price', '$description', '$short_description', '$full_path_1', '$full_path_2', '$full_path_3', '$full_path_4', '$short_name')";
            $result = mysqli_query($connect, $query);
        }
    ?>
</body>
</html>